#pragma once

#include <glm/glm.hpp>
#include <common/common_functions.cuh>

void grayscale(const glm::u8vec4* src_image, glm::u8vec4* dst_image, ImageWorkArea srcWorkArea, ImageWorkArea dstWorkArea, glm::uvec2 blockDim);
