#include "CudaTimeMeasurement.h"

#include "CudaError.h"

#include <cuda_runtime_api.h>
#include <cassert>

CudaTimeMeasurement::~CudaTimeMeasurement() {
	cudaEventDestroy(startEvent);
	cudaEventDestroy(stopEvent);
}

void CudaTimeMeasurement::start() {
	switch (status) {
		case Uninitialized:
			cudaEventCreate(&startEvent);
			cudaEventCreate(&stopEvent);
		case Finished:
			cudaEventRecord(startEvent);
			status = Started;
			break;
		default:
			assert(false);
	}
}

float CudaTimeMeasurement::stop() {
	assert(status == Started);

	lastTimeSeconds = 0.0f;
	status = Invalid;

	checkCudaErrors(cudaEventRecord(stopEvent));
	checkCudaErrors(cudaEventSynchronize(stopEvent));
	float ms;
	checkCudaErrors(cudaEventElapsedTime(&ms, startEvent, stopEvent));
	lastTimeSeconds = ms * 1e-3f;

	status = Finished;

	return lastTimeSeconds;
}

float CudaTimeMeasurement::getSeconds() {
	return lastTimeSeconds;
}
