file(GLOB COMMON_SRC_FILES
		${CMAKE_CURRENT_SOURCE_DIR}/common/*.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/common/*.h
		)

set(RENDERER_SRC_FILES
		${COMMON_SRC_FILES}
		Heightmap.cpp
		app.cpp
		)

set(GENERATOR_SRC_FILES
		${COMMON_SRC_FILES}
		Heightmap.cpp
		mapgen.cpp
		)

#message(WARNING "${COMMON_SRC_FILES}")

MAKE_OPENGL_TASK(595Nasibullin 2 "${RENDERER_SRC_FILES}")
MAKE_OPENGL_TASK(595Nasibullin_HeightmapGenerator 2  "${GENERATOR_SRC_FILES}")
add_dependencies(595Nasibullin_HeightmapGenerator2 595Nasibullin2_Data2)

target_include_directories(595Nasibullin2 PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/common/)
target_include_directories(595Nasibullin_HeightmapGenerator2 PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/common/)

