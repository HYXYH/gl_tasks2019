//
// Created by Оскар on 03.04.19.
//

#include "Heightmap.h"
#include <string>

int main(int argc, char *argv[]){
	std::string path(argv[0]);
	std::size_t found = path.find_last_of("/\\");
	path = path.substr(0,found);
	path.append("/595NasibullinData2/images/heightmaps/planet_1_200_7_16_sq_160.png");

	Heightmap h;
//	h.readFlatMapFromImage(path);

//	h.generateFlatMap(1, 129, 60);
//	h.normalizeHeights();
//	h.SqrHeightmap();
//	h.scaleHeights(25);

	h.generateSphereMap(1,200, 7, 16);
	h.normalizeHeights();
	h.SqrHeightmap();
	h.scaleHeights(160);

	h.generateAndSaveBiomeTexture("/Users/Oskar/Desktop/biomesmap.png");
	h.saveToPNG("/Users/Oskar/Desktop/heightmap.png");
	h.saveToObj("/Users/Oskar/Desktop/generated.obj", 0.2);

	h.getHeightOnSphereMap(1,0,0);
	h.getHeightOnSphereMap(0,1,0);
	h.getHeightOnSphereMap(0,-1,0);
//	h.getHeightOnSphereMap(0,0,-10);
	return 0;
}