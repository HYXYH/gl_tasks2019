#include <Camera.hpp>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/projection.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

#include <iostream>

void OrbitCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void OrbitCameraMover::showOrientationParametersImgui() {
    ImGui::Text("r = %.2f, phi = %.2f, theta = %2f", _r, _phiAng, _thetaAng);
}

void OrbitCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        _phiAng -= dx * 0.005;
        _thetaAng += dy * 0.005;

        _thetaAng = glm::clamp(_thetaAng, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void OrbitCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
    _r += _r * yoffset * 0.05;
}

void OrbitCameraMover::update(GLFWwindow* window, double dt)
{
    double speed = 1.0;

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _phiAng -= speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _phiAng += speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _thetaAng += speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _thetaAng -= speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
    {
        _r += _r * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
    {
        _r -= _r * dt;
    }

    _thetaAng = glm::clamp(_thetaAng, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);

    //-----------------------------------------

    //Вычисляем положение виртуальной камеры в мировой системе координат по формуле сферических координат
    glm::vec3 pos = glm::vec3(glm::cos(_phiAng) * glm::cos(_thetaAng), glm::sin(_phiAng) * glm::cos(_thetaAng), glm::sin(_thetaAng) + 0.5f) * (float)_r;

    //Обновляем матрицу вида
    _camera.viewMatrix = glm::lookAt(pos, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f));

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}

glm::vec3 OrbitCameraMover::getPosition() {
	return glm::vec3(_phiAng, _thetaAng, _r);
}

//=============================================

FreeCameraMover::FreeCameraMover() :
CameraMover(),
_pos(5.0f, 0.0f, -47.5f)
{       
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(4.0f, 0.0f, -47.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
}

void FreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void FreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS || _alwaysControl)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз        
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir = glm::vec3(0.0f, 1.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void FreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void FreeCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 5.0f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        
    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _pos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _pos += rightDir * speed * static_cast<float>(dt);
    }

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		std::cout << "\nLOL\n";
		_alwaysControl = !_alwaysControl;
	}


	// вращаем по оси взгляда

	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
	{
		_rot *= glm::angleAxis(static_cast<float>(speed * 0.005), forwDir);
	}
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
	{
		_rot *= glm::angleAxis(static_cast<float>(-speed * 0.005), forwDir);
	}

    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);
    
    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}

glm::vec3 FreeCameraMover::getPosition() {
	return _pos;
}

void FreeCameraMover::copyInternalData(CameraMover* cam)
{
	_pos = ((FreeCameraMover*)cam)->_pos;
	_rot = ((FreeCameraMover*)cam)->_rot;
}


//=============================================

PlanetSurfaceCameraMover::PlanetSurfaceCameraMover(glm::vec3 planetPos, std::string heightmapPath) {
	_pos = glm::vec3(0.0f, -10.0f, 5.0f);
	_planetPos = planetPos;
	_heightmap.readFlatMapFromImage(heightmapPath);
	_rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(-1.0f, -9.0f, 5.0f), glm::vec3(-0.0f, 0.0f, -1.0f)));
}

void PlanetSurfaceCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
	int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	if (state == GLFW_PRESS  || _alwaysControl)
	{
		double dx = xpos - _oldXPos;
		double dy = ypos - _oldYPos;

		//Добавляем небольшой поворот вверх/вниз
		glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
		_rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

		//Добавляем небольшой поворов вокруг вертикальной оси
		glm::vec3 upDir = glm::normalize(_pos - _planetPos);
		_rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);

	}

	_oldXPos = xpos;
	_oldYPos = ypos;
}

void PlanetSurfaceCameraMover::update(GLFWwindow* window, double dt)
{


	//Получаем текущее направление "вперед" в мировой системе координат
	glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

	//Получаем текущее направление "вправо" в мировой системе координат
	glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

	//Двигаем камеру вперед/назад
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		_pos += forwDir * _speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		_pos -= forwDir * _speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		_pos -= rightDir * _speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		_pos += rightDir * _speed * static_cast<float>(dt);
	}

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		std::cout << "\nLOL\n";
		_alwaysControl = !_alwaysControl;
	}

	//-----------------------------------------

	//высота относительно центра планеты
	float planetScale = 0.2;
	glm::vec3 newPos = _pos - _planetPos;
	float currHeight = glm::length(newPos);
	surfaceHeight = _heightmap.getHeightOnSphereMap(newPos.x, newPos.y, newPos.z) * planetScale + 1;
	float dh = (currHeight - surfaceHeight) * _speed * static_cast<float>(dt);

	_pos = glm::normalize(newPos) * (currHeight-dh) + _planetPos;
	//-----------------------------------------

	// fix camera roll
	glm::vec3 fixUp = glm::normalize(_pos - _planetPos);
	glm::vec3 projRight = glm::normalize(rightDir - glm::proj(rightDir, fixUp));
	glm::vec3 axis = glm::normalize(glm::cross(projRight,rightDir));

	_roll = acos(glm::dot(projRight, rightDir));
	if (isnan(_roll) || _roll < _minFixAngle){
		_roll = 0;
	}
	else
	{
		glm::vec3 cross = glm::cross(projRight, rightDir);
		if (glm::dot(axis, cross) < 0) {
			_roll = - _roll;
		}
		glm::quat fixRot = glm::angleAxis(_roll * _fixRotationSpeed * (float) dt, axis);
		_rot = _rot * fixRot;
	}


	//Соединяем перемещение и поворот вместе
	_camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

	//-----------------------------------------

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	//Обновляем матрицу проекции на случай, если размеры окна изменились
	_camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}


float PlanetSurfaceCameraMover::getRollAngle(){
	return _roll;
}