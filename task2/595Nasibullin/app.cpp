#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <sstream>
#include <vector>

/**
Пример с кубической текстурой
*/
class SampleApplication : public Application
{
public:
    MeshPtr _planet;
    MeshPtr _backgroundCube;

    glm::vec3 _planetPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    bool debugPrint = false;

    MeshPtr _marker; //Меш - маркер для источника света

    //Идентификатор шейдерной программы
    ShaderProgramPtr _wireframeShader;
    ShaderProgramPtr _commonShader;
    ShaderProgramPtr _markerShader;
    ShaderProgramPtr _skyboxShader;

    //Переменные для управления положением одного источника света
    float _lr = 46.0;
    float _phi = 6.0f;
    float _theta = 4.36f;

	int _tileCount = 100;

    LightInfo _light;
    glm::vec4 _wireframeColor = glm::vec4(1.0, 0.4, 0.3, 1.0);
    float _wireframeWidth = 1.0f;

    TexturePtr _snowTex;
    TexturePtr _grassTex;
    TexturePtr _rockTex;
    TexturePtr _dirtTex;
    TexturePtr _biomesTex;

    TexturePtr _cubeTex;

    GLuint _sampler;
    GLuint _cubeTexSampler;

    CameraMoverPtr _freeCameraMover;
    CameraMoverPtr _surfCameraMover;
    enum CameraMode {
        FreeCam = 0,
        SurfaceCam = 1
    } cameraMode = FreeCam;

    enum LightMode {
        PointLight = 0,
        DirectionalLight = 1
    } lightMode = DirectionalLight;

    enum RenderMode {
        PlanetRender = 0,
        WireframeRender = 1,
        BothRender = 2
    } renderMode = PlanetRender;

    SampleApplication(){
        _freeCameraMover = std::make_shared<FreeCameraMover>();
        _surfCameraMover = std::make_shared<PlanetSurfaceCameraMover>(_planetPosition, "595NasibullinData2/images/heightmaps/planet_1_200_7_16_sq_160.png");
        _cameraMover = _freeCameraMover;
    }

    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей

      // вроде если умножить радиус на 2, то размер полигонов для планеты будет идеальным (в 4 раза увеличатся треугольники)
        _planet = loadFromFile("595NasibullinData2/models/planet_1_200_7_16_sq_160.obj");
        _planet->setModelMatrix(glm::translate(glm::mat4(1.0f), _planetPosition));
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        _marker = makeSphere(0.1f);
        _backgroundCube = makeCube(50.0f);


        //=========================================================
        //Инициализация шейдеров
        _wireframeShader = std::make_shared<ShaderProgram>("595NasibullinData2/shaders/simple.vert", "595NasibullinData2/shaders/simple.frag");
        _commonShader = std::make_shared<ShaderProgram>("595NasibullinData2/shaders/common.vert", "595NasibullinData2/shaders/common.frag");
        _markerShader = std::make_shared<ShaderProgram>("595NasibullinData2/shaders/marker.vert", "595NasibullinData2/shaders/marker.frag");
        _skyboxShader = std::make_shared<ShaderProgram>("595NasibullinData2/shaders/skybox.vert", "595NasibullinData2/shaders/skybox.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.31, 0.2, 0.6);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур

        _snowTex = loadTexture("595NasibullinData2/images/snow.png");
        _grassTex = loadTexture("595NasibullinData2/images/grass.jpg");
        _rockTex = loadTexture("595NasibullinData2/images/rock1.png");
        _dirtTex = loadTexture("595NasibullinData2/images/grass-and-rock.png");
        _biomesTex = loadTexture("595NasibullinData2/images/biomesmap.png");

        _cubeTex = loadCubeTextureHDR("595NasibullinData2/skyboxes/pn2hdr");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_cubeTexSampler);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

        /// !!!!!!!!!!!!!!
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_POLYGON_OFFSET_FILL);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("595 Nasibullin Task 2", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::Text("Light mode");
                ImGui::RadioButton("Point", reinterpret_cast<int*>(&lightMode), static_cast<int>(PointLight));
                ImGui::RadioButton("Directional", reinterpret_cast<int*>(&lightMode), static_cast<int>(DirectionalLight));

                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 1.0f, 100.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, 2 * glm::pi<float>());
            }

            ImGui::Text("Camera mode");
            ImGui::RadioButton("Free cam", reinterpret_cast<int*>(&cameraMode), static_cast<int>(FreeCam));
            ImGui::RadioButton("Surface cam", reinterpret_cast<int*>(&cameraMode), static_cast<int>(SurfaceCam));
	        ImGui::SliderInt("Tile count", &_tileCount, 1, 500);

            glm::vec3 pos = _cameraMover->getPosition();
            ImGui::Text("Cam Position: %.1f %.1f %.1f", pos.x, pos.y, pos.z );
            ImGui::Text("Cam Roll: %.4f", _cameraMover->getRollAngle() );

            if (cameraMode == CameraMode::SurfaceCam) {
                ImGui::Text("Surface height: %.2f", ((PlanetSurfaceCameraMover *) _cameraMover.get())->surfaceHeight);
            }

            if (ImGui::CollapsingHeader("WireFrame")) {
                ImGui::Text("Objects to render");
                ImGui::RadioButton("Planet", reinterpret_cast<int *>(&renderMode), static_cast<int>(PlanetRender));
                ImGui::RadioButton("Wireframe", reinterpret_cast<int *>(&renderMode), static_cast<int>(WireframeRender));
                ImGui::RadioButton("Both", reinterpret_cast<int *>(&renderMode), static_cast<int>(BothRender));

                GLfloat lineWidthRange[2] = {0.0f, 0.0f};
                glGetFloatv(GL_SMOOTH_LINE_WIDTH_RANGE, lineWidthRange);
                ImGui::SliderFloat("width", &_wireframeWidth, lineWidthRange[0], lineWidthRange[1]);
                ImGui::ColorEdit3("color", glm::value_ptr(_wireframeColor));
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        // make camera great again
        if(cameraMode == CameraMode::FreeCam && _cameraMover != _freeCameraMover){
            _cameraMover = _freeCameraMover;
            _freeCameraMover->copyInternalData(_surfCameraMover.get());
        }
        if (cameraMode == CameraMode::SurfaceCam && _cameraMover != _surfCameraMover){
            _cameraMover = _surfCameraMover;
            _surfCameraMover.get()->copyInternalData(_freeCameraMover.get());
        }

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


      //====== РИСУЕМ ФОН С КУБИЧЕСКОЙ ТЕКСТУРОЙ ======
      {
        _skyboxShader->use();

        glm::vec3 cameraPos = glm::vec3(glm::inverse(_camera.viewMatrix)[3]); //Извлекаем из матрицы вида положение виртуальный камеры в мировой системе координат

        _skyboxShader->setVec3Uniform("cameraPos", cameraPos);
        _skyboxShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _skyboxShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Для преобразования координат в текстурные координаты нужна специальная матрица
        glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        _skyboxShader->setMat3Uniform("textureMatrix", textureMatrix);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _cubeTexSampler);
        _cubeTex->bind();
        _skyboxShader->setIntUniform("cubeTex", 0);

        glDepthMask(GL_FALSE); //Отключаем запись в буфер глубины

        _backgroundCube->draw();

        glDepthMask(GL_TRUE); //Включаем обратно запись в буфер глубины
      }


        //====== РИСУЕМ ОСНОВНЫЕ ОБЪЕКТЫ СЦЕНЫ ======
        _commonShader->use();
        _commonShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _commonShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;

        glm::vec3 lightPosCamSpace;
        if(lightMode == LightMode::PointLight)
        {
            lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
            _commonShader->setIntUniform("light.w", 1); //копируем положение уже в системе виртуальной камеры
        }
        else
        {
            lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 0.0));
            _commonShader->setIntUniform("light.w", 0); //копируем положение уже в системе виртуальной камеры
        }
        _commonShader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _commonShader->setVec3Uniform("light.La", _light.ambient);
        _commonShader->setVec3Uniform("light.Ld", _light.diffuse);
        _commonShader->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _sampler);
        _snowTex->bind();
        _commonShader->setIntUniform("snowTex", 1);

        glActiveTexture(GL_TEXTURE2);
        glBindSampler(2, _sampler);
        _grassTex->bind();
        _commonShader->setIntUniform("grassTex", 2);

        glActiveTexture(GL_TEXTURE3);
        glBindSampler(3, _sampler);
        _rockTex->bind();
        _commonShader->setIntUniform("rockTex", 3);

        glActiveTexture(GL_TEXTURE4);
        glBindSampler(4, _sampler);
        _dirtTex->bind();
        _commonShader->setIntUniform("dirtTex", 4);

        glActiveTexture(GL_TEXTURE5);
        glBindSampler(5, _sampler);
        _biomesTex->bind();
        _commonShader->setIntUniform("mapTex", 5);

        if(renderMode == RenderMode::PlanetRender || renderMode == RenderMode::BothRender)
        {
            _commonShader->setMat4Uniform("modelMatrix", _planet->modelMatrix());
            _commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _planet->modelMatrix()))));
	        _commonShader->setIntUniform("tileCount", _tileCount);

	        glPolygonOffset(5, 5);
            _planet->draw();
        }

        //рисуем wireframe
        if(renderMode == RenderMode::WireframeRender || renderMode == RenderMode::BothRender)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glEnable(GL_LINE_SMOOTH);
            glLineWidth(_wireframeWidth);
            glPolygonOffset(-5, -5);
            _wireframeShader->use();
            _wireframeShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
            _wireframeShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
	        _wireframeShader->setMat4Uniform("modelMatrix", _planet->modelMatrix());
            _wireframeShader->setVec4Uniform("color", _wireframeColor);
            _planet->draw();
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glDisable(GL_LINE_SMOOTH);
            glPolygonOffset(5, 5);
        }

        //Рисуем маркеры для всех источников света
        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}