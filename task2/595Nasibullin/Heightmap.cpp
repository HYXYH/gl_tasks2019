//
// Created by Оскар on 30.03.19.
//

#include <SOIL2.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <set>
#include <map>
#include "Heightmap.h"

bool Heightmap::lrflag = false;


float dotproduct(Vertex v1, Vertex v2){
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

float vectLen(Vertex v){
	return (float)sqrt(dotproduct(v,v));
}

float distance(Vertex v1, Vertex v2){
	return (float)sqrt((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y) + (v1.z - v2.z) * (v1.z - v2.z));
}

Vertex divideLine(Vertex a, Vertex b){
//	a + (b-a)/2
	Vertex dir{(b.x - a.x)/2, (b.y - a.y)/2, (b.z - a.z)/2};
	return Vertex{a.x + dir.x, a.y + dir.y, a.z + dir.z };
}

bool is_file_exist(const char *fileName)
{
	std::ifstream infile(fileName);
	return infile.good();
}

//todo: fixme!!
float randomRange(double min, double max){
	int range = max - min + 1;
	return  rand() % range + min;
}





void Heightmap::readFlatMapFromImage(std::string path) {

	if (!is_file_exist(path.c_str())) {
		std::cerr << path.c_str() << " not found! \n";
	}

	int channels;
	unsigned char *image = SOIL_load_image(path.c_str(), &_xsize, &_ysize, &channels, SOIL_LOAD_AUTO);
	if (!image) {
		std::cerr << "SOIL loading error: " << SOIL_last_result() << std::endl;
		return;
	}

	if (channels == 1) {
		float scale = 1;
		for (int j = 0; j < _ysize; ++j) {
			std::vector<float> row(_xsize);
			for (int i = 0; i < _xsize; i++) {
				float val = float(image[_xsize * j + i]) * scale;
				row.push_back(val);
			}
			_heightMap.push_back(row);
		}
	}
	else if (channels == 4){
		for (int i = 0; i < _ysize; ++i) {
			std::vector<float> row;
			for (int j = 0; j < _xsize; j++) {
				float* val = (float*) &image[(_xsize * i + j ) * channels];
				row.push_back(*val);
			}
			_heightMap.push_back(row);
		}
	}
	else{
		std::cerr << "unknown map type";
	}

	SOIL_free_image_data(image);

	_mapType = FLAT_MAP;
}

void Heightmap::saveToObj(std::string path, float scale) {
	if(_mapType == NO_MAP){
		std::cerr<< "no map to save";
		return;
	}

	std::ofstream fout(path, std::ofstream::out);
	fout << "o Heightmap";
	long triangles = 0;
	long vertices = 0;

	if(_mapType == FLAT_MAP) {
		for (int j = 0; j < _ysize; ++j) {
			for (int i = 0; i < _xsize; i++) {
				fout << "\nv " << j * scale << " " << i * scale << " " << _heightMap[i][j] * scale;
				vertices++;
			}
		}

		for (int j = 0; j < _ysize - 1; ++j) {
			for (int i = 1; i < _xsize; i++) {
				int offset = j * _xsize;
				// up triangle
				fout << "\nf " << i + offset << " " << i + 1 + offset << " " << i + _xsize + offset;

				//down triangle
				fout << "\nf " << _xsize + i + offset << " " << i + 1 + offset << " " << _xsize + i + 1 + offset;

				triangles += 2;
			}
		}
		std::cout << "\nvertices: "<< vertices << "\ntriangles: "<< triangles;
	}
	else if(_mapType == SPHERE_MAP){
		for(int i = 0; i<_vertices.size(); i++){
			fout << "\nv " << _vertices[i].x * scale << " " << _vertices[i].y * scale << " " << _vertices[i].z * scale;
		}
		if (_uvs.size() > 0){
			for(int i = 0; i<_uvs.size(); i++){
				fout << "\nvt " << _uvs[i].x << " " << _uvs[i].y;
			}
			for(int i = 0; i<_triangles.size(); i++){
				fout << "\nf "
				     << _triangles[i].v1 + 1 << "/" << _triangles[i].v1 + 1 << " "
				     << _triangles[i].v2 + 1 << "/" << _triangles[i].v2 + 1 << " "
				     << _triangles[i].v3 + 1 << "/" << _triangles[i].v3 + 1;
			}
		}
		else {
			for (int i = 0; i < _triangles.size(); i++) {
				fout << "\nf " << _triangles[i].v1 + 1 << " " << _triangles[i].v2 + 1 << " " << _triangles[i].v3 + 1;
			}
		}
		std::cout << "\nvertices: "<< _vertices.size() << "\ntriangles: "<< _triangles.size();
	}
	else{
		std::cerr<< "bad map type";
		return;
	}
	fout.close();
}


void Heightmap::generateFlatMap(unsigned int seed, int size, double roughness) {
	if ((size - 1) & (size - 2) != 0) {
		std::cerr << "size must be (2^n) + 1";
		return;
	}

	_mapType = NO_MAP;
	_roughness = roughness;
	srand(seed);
	_ysize = size;
	_xsize = _ysize * 2 - 1;

	_heightMap = std::vector<std::vector<float>>(_xsize, std::vector<float>(_ysize));

	_heightMap[0][0] = randomRange(0.3f, 0.6f);
	_heightMap[0][_ysize - 1] = randomRange(0.3f, 0.6f);
	_heightMap[_xsize - 1][_ysize - 1] = randomRange(0.3f, 0.6f);
	_heightMap[_xsize - 1][0] = randomRange(0.3f, 0.6f);

	_heightMap[_ysize - 1][_ysize - 1] = randomRange(0.3f, 0.6f);
	_heightMap[_ysize - 1][0] = randomRange(0.3f, 0.6f);

	for (int l = (_ysize - 1) / 2; l > 0; l /= 2) {
		for (int x = 0; x < _xsize - 1; x += l) {
			if (x >= _ysize - l)
				lrflag = true;
			else
				lrflag = false;

			for (int y = 0; y < _ysize - 1; y += l) {
				diamondSquare(x, y, x + l, y + l);
			}
		}
	}
	_mapType = FLAT_MAP;
}

void Heightmap::diamondSquare(int lx, int ly, int rx, int ry)
{
	int l = (rx - lx) / 2;

	square(lx, ly, rx, ry);

	diamond(lx, ly + l, l);
	diamond(rx, ry - l, l);
	diamond(rx - l, ry, l);
	diamond(lx + l, ly, l);
}

void Heightmap::diamond(int tgx, int tgy, int l) {
	float a, b, c, d;

	if (tgy - l >= 0)
		a = _heightMap[tgx][tgy - l];                         //      C--------
	else                                                      //      |        |
		a = _heightMap[tgx][_ysize - l];                      // B---t g----D  |
															  //      |        |
															  //      A--------
	if (tgx - l >= 0)
		b = _heightMap[tgx - l][tgy];
	else
	if (lrflag)
		b = _heightMap[_xsize - l][tgy];
	else
		b = _heightMap[_ysize - l][tgy];


	if (tgy + l < _ysize)
		c = _heightMap[tgx][tgy + l];
	else
		c = _heightMap[tgx][l];

	if (lrflag)
		if (tgx + l < _xsize)
			d = _heightMap[tgx + l][tgy];
		else
			d = _heightMap[l][tgy];
	else
	if (tgx + l < _ysize)
		d = _heightMap[tgx + l][tgy];
	else
		d = _heightMap[l][tgy];

	_heightMap[tgx][tgy] = (a + b + c + d) / 4 + randomRange(-l * 2 * _roughness / _ysize, l * 2 * _roughness / _ysize);
}

void Heightmap::square(int lx, int ly, int rx, int ry) {
	int l = (rx - lx) / 2;

	float a = _heightMap[lx][ly];              //  B--------C
	float b = _heightMap[lx][ry];              //  |        |
	float c = _heightMap[rx][ry];              //  |   ce   |
	float d = _heightMap[rx][ly];              //  |        |
	int cex = lx + l;                          //  A--------D
	int cey = ly + l;

	_heightMap[cex][cey] = (a + b + c + d) / 4 + randomRange(-(l * 2 * _roughness / _ysize), (l * 2 * _roughness / _ysize));
}

void Heightmap::normalizeHeights()
{
	if (_mapType == FLAT_MAP) {
		double min(0), max(0);
		for (int x = 0; x < _xsize; x++)
			for (int y = 0; y < _ysize; y++) {
				if (_heightMap[x][y] > max)
					max = _heightMap[x][y];
				if (_heightMap[x][y] < min)
					min = _heightMap[x][y];
			}

		double range = max - min;
		for (int x = 0; x < _xsize; x++)
			for (int y = 0; y < _ysize; y++) {
				_heightMap[x][y] -= min;
				_heightMap[x][y] /= range;
			}
	}
	if (_mapType == SPHERE_MAP) {
		double min(vectLen(_vertices[0])), max(vectLen(_vertices[0]));
		for (int i = 1; i < _vertices.size(); i++) {
			float len = vectLen(_vertices[i]);
			if (len > max)
				max = len;
			if (len < min)
				min = len;
		}

		double range = max - min;
		for (int i = 0; i < _vertices.size(); i++) {
			float len = vectLen(_vertices[i]);
			float height = ((len - min) / range) - 0.5 + _radius;


			_vertices[i] = Vertex{
				(_vertices[i].x / len) * height,
				(_vertices[i].y / len) * height,
				(_vertices[i].z / len) * height};

		}
//		std::cout << "generated:\n\trange: " << range << "\n\tmin: "<<min<<"\n\tmax: "<<max <<"\n";
//
//
//		min = vectLen(_vertices[0]);
//		max = vectLen(_vertices[0]);
//		for (int i = 1; i < _vertices.size(); i++) {
//			float len = vectLen(_vertices[i]);
//			if (len > max)
//				max = len;
//			if (len < min)
//				min = len;
//		}
//		range = max - min;
//		std::cout << "normalized:\n\trange: " << range << "\n\tmin: "<<min<<"\n\tmax: "<<max <<"\n";
	}


}

void Heightmap::SqrHeightmap()
{
	//use only if map heights are in range [0,1] (use normalizeHeights)
	if (_mapType == FLAT_MAP) {
		for (int x = 0; x < _xsize; x++)
			for (int y = 0; y < _ysize; y++)
				_heightMap[x][y] *= _heightMap[x][y];
	}
	if (_mapType == SPHERE_MAP) {
		for (int i = 0; i < _vertices.size(); i++) {
			float len = vectLen(_vertices[i]) ;
			float normLen = len - _radius;
			float height = normLen * fabsf(normLen)  + _radius;

			_vertices[i] = Vertex{
				(_vertices[i].x / len) * height,
				(_vertices[i].y / len) * height,
				(_vertices[i].z / len) * height};
		}
	}
}

void Heightmap::scaleHeights(float scale) {
	if (_mapType == FLAT_MAP) {
		for (int x = 0; x < _xsize; x++)
			for (int y = 0; y < _ysize; y++)
				_heightMap[x][y] *= scale;
	}
	if (_mapType == SPHERE_MAP) {
		double min(vectLen(_vertices[0])), max(vectLen(_vertices[0]));
		for (int i = 1; i < _vertices.size(); i++) {
			float len = vectLen(_vertices[i]);
			if (len > max)
				max = len;
			if (len < min)
				min = len;
		}

		double range = max - min;
		for (int i = 0; i < _vertices.size(); i++) {
			float len = vectLen(_vertices[i]);
			float height = (len - min - range/2) * scale + _radius;

			_vertices[i] = Vertex{
				(_vertices[i].x / len) * height,
				(_vertices[i].y / len) * height,
				(_vertices[i].z / len) * height};
		}
//		std::cout << "Squared:\n\trange: " << range << "\n\tmin: "<<min<<"\n\tmax: "<<max <<"\n";
//
//		min = vectLen(_vertices[0]);
//		max = vectLen(_vertices[0]);
//		for (int i = 1; i < _vertices.size(); i++) {
//			float len = vectLen(_vertices[i]);
//			if (len > max)
//				max = len;
//			if (len < min)
//				min = len;
//		}
//		range = max - min;
//		std::cout << "Scaled heights:\n\trange: " << range << "\n\tmin: "<<min<<"\n\tmax: "<<max <<"\n";
	}
}



// generating by octohedron subdivision
void Heightmap::generateSphereMap(unsigned int seed, float radius, int subdivDepth, double roughness)
{
	_radius = radius;
	_roughness = roughness;
//	initial octohedron
	_vertices.push_back(Vertex{radius,0,0});
	_vertices.push_back(Vertex{-radius,0,0});
	_vertices.push_back(Vertex{0,radius,0});
	_vertices.push_back(Vertex{0,-radius,0});
	_vertices.push_back(Vertex{0,0,radius});
	_vertices.push_back(Vertex{0,0,-radius});

	_tmpTriangles.push_back(Triangle{2,4,0});
	_tmpTriangles.push_back(Triangle{0,4,3});
	_tmpTriangles.push_back(Triangle{3,4,1});
	_tmpTriangles.push_back(Triangle{1,4,2});
	_tmpTriangles.push_back(Triangle{0,5,2});
	_tmpTriangles.push_back(Triangle{3,5,0});
	_tmpTriangles.push_back(Triangle{1,5,3});
	_tmpTriangles.push_back(Triangle{2,5,1});

	if(subdivDepth == 0)
	{
		_triangles = _tmpTriangles;
	}
	else
	{
		for (int i = 0; i < 8; i++) {
			subdivide(i, subdivDepth);
		}
	}
	_tmpTriangles.clear();
	_dividedLines.clear();
	unsigned long counter = 0;
	std::map<Vertex, unsigned long> vert_ids;
	for(int i = 0; i<_triangles.size(); i++) {
		unsigned long* ids[3] = {&_triangles[i].v1, &_triangles[i].v2, &_triangles[i].v3};
		for(int j = 0; j<3; j++) {
			auto found = vert_ids.find(_vertices[*ids[j]]);
			if (found == vert_ids.end()) {
				vert_ids.insert(std::make_pair(_vertices[*ids[j]], counter));
				*ids[j] = counter;
				counter++;
			} else {
				*ids[j] = found->second;
			}
		}
	}

	_vertices = std::vector<Vertex>(vert_ids.size());
	for(std::map<Vertex, unsigned long>::iterator it = vert_ids.begin(); it != vert_ids.end(); ++it) {
		_vertices[it->second] = it->first;
	}

	std::cout<<"\nSphereMap: generated " << _vertices.size() << " vertices\n";
	_mapType = SPHERE_MAP;
}


Vertex Heightmap::midPointDisplacement1D(Vertex a, Vertex mid, Vertex b){

	float heightA = vectLen(a);
	float heightMid = vectLen(mid);
	float heightB = vectLen(b);
	float l = distance(a,b);

	float height = (heightA + heightB) / 2 + randomRange(-(l * 2 * _roughness/_radius), (l * 2 * _roughness/_radius));
	height /= heightMid;

	return Vertex{mid.x * height, mid.y * height, mid.z * height};
}

unsigned long Heightmap::getUniqMidId(Vertex v1, Vertex v2){
	auto found = _dividedLines.find(std::make_pair(v1, v2));
	if (found == _dividedLines.end()){
		found = _dividedLines.find(std::make_pair(v2, v1));
	}

	if (found == _dividedLines.end()) {
		Vertex vd1 = midPointDisplacement1D(v1, divideLine(v1, v2), v2);
		_vertices.push_back(vd1);

		_dividedLines.insert(std::make_pair(std::make_pair(v1, v2), _vertices.size()-1));
		return _vertices.size()-1;
	}
	else{
		return found->second;
	}
}


void Heightmap::subdivide(unsigned long triangle_idx, int depth) {
	Vertex v1 = _vertices[_tmpTriangles[triangle_idx].v1];
	Vertex v2 = _vertices[_tmpTriangles[triangle_idx].v2];
	Vertex v3 = _vertices[_tmpTriangles[triangle_idx].v3];

	unsigned long vd1id = getUniqMidId(v2, v3);
	unsigned long vd2id = getUniqMidId(v1, v3);
	unsigned long vd3id = getUniqMidId(v1, v2);

	if(depth == 1){
		_triangles.push_back(Triangle{vd1id, vd2id, vd3id});
		_triangles.push_back(Triangle{_tmpTriangles[triangle_idx].v1, vd3id, vd2id});
		_triangles.push_back(Triangle{_tmpTriangles[triangle_idx].v2, vd1id, vd3id});
		_triangles.push_back(Triangle{_tmpTriangles[triangle_idx].v3, vd2id, vd1id});
		return;
	}

	_tmpTriangles.push_back(Triangle{vd1id, vd2id, vd3id});
	_tmpTriangles.push_back(Triangle{_tmpTriangles[triangle_idx].v1, vd3id, vd2id});
	_tmpTriangles.push_back(Triangle{_tmpTriangles[triangle_idx].v2, vd1id, vd3id});
	_tmpTriangles.push_back(Triangle{_tmpTriangles[triangle_idx].v3, vd2id, vd1id});

	unsigned long lastid = _tmpTriangles.size()-1;

	depth--;
	subdivide(lastid, depth);
	subdivide(lastid-1, depth);
	subdivide(lastid-2, depth);
	subdivide(lastid-3, depth);
}


void Heightmap::fillUvsAndHeightmapFromSphereMap() {
	if (_mapType == SPHERE_MAP) {
		int size = int(sqrt(_vertices.size()));
		_heightMap = std::vector<std::vector<float>>(size, std::vector<float>(size, 0));
		_uvs = std::vector<Vertex>(_vertices.size());
		for (int i = 0; i < _vertices.size(); i++) {
			Vertex v = _vertices[i];
			float vlen = vectLen(v);
			v = Vertex{v.x / vlen, v.y / vlen, v.z / vlen};
			Vertex textureCoordinates;
			textureCoordinates.x = atan2(v.x, v.z) / (-2.0 * M_PI);
			if (textureCoordinates.x < 0) {
				textureCoordinates.x += 1;
			}
			textureCoordinates.y = asin(v.y) / M_PI + 0.5;
			textureCoordinates.z = vlen;

			_uvs[i] = textureCoordinates;
			_heightMap[(int) (textureCoordinates.x * size)][(int) (textureCoordinates.y * size)] = vlen;
		}

		// try to fix zero points
		{
			int zeros(0), last_zeros(0);
			do {
				last_zeros = zeros;
				zeros = 0;
				for (int i = 1; i < size - 1; i++) {
					for (int j = 1; j < size - 1; j++) {
						if (_heightMap[i][j] < 0.1) {
							float neib[4] = {
								_heightMap[i + 1][j],
								_heightMap[i - 1][j],
								_heightMap[i][j + 1],
								_heightMap[i][j - 1]};
							int neibZeros = 0;
							for (int k = 0; k < 4; k++) {
								if (neib[k] < 0.1) {
									neibZeros++;
								}
							}
							if (neibZeros > 2) {
								zeros++;
							} else {
								_heightMap[i][j] = (neib[0] + neib[1] + neib[2] + neib[3]) / (4 - neibZeros);
							}
						}
					}
				}
			} while (zeros != last_zeros);
		}
	}
}


void Heightmap::saveToPNG(std::string path) {
	if(_mapType == NO_MAP){
		std::cerr<< "no map to save";
		return;
	}
	if (_mapType == SPHERE_MAP) {
		int size = int(sqrt(_vertices.size()));
		fillUvsAndHeightmapFromSphereMap();

		std::vector<int> test(_vertices.size());
		std::vector<unsigned char> buf(size * size * 4);
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				Vertex v = _vertices[i];
				int idx = (i * size + j) * 4;
				// wtf: [i][j] or [j][i] ?
				float tmp = _heightMap[i][j];
				if(tmp < 0.1){
					tmp = _radius;
				}
				unsigned char *s = (unsigned char *) &(tmp);

				buf[idx] = s[0];
				buf[idx + 1] = s[1];
				buf[idx + 2] = s[2];
				buf[idx + 3] = s[3];
				test[idx] = 1;
			}
		}

		int err = SOIL_save_image
			(
				path.c_str(),
				SOIL_SAVE_TYPE_PNG,
				size, size, 4,
				&buf[0]
			);
		if (err != 0) {
			std::cerr<<"SOIL: " << SOIL_last_result() << "\n";
		}

		int count = 0;
		for (int i = 0; i < test.size(); i++) {
			count+= test[i];
		}
		std::cout << "verts: " << _vertices.size() << "\ncount: " << test.size();

	}
	else
	{
		std::cerr<< "bad map type";
		return;
	}
}

float Heightmap::getHeightOnSphereMap(float x, float y, float z) {
	Vertex v{x,y,z};
	float vlen = vectLen(v);
	v = Vertex{v.x / vlen, v.y / vlen, v.z / vlen};
	Vertex textureCoordinates;
	textureCoordinates.x = atan2(v.x, v.z) / (-2.0 * M_PI);
	if (textureCoordinates.x < 0) {
		textureCoordinates.x += 1;
	}
	textureCoordinates.y = asin(v.y) / M_PI + 0.5;
	int size = _heightMap.size();

	int ix = (int)(textureCoordinates.x * size);
	int iy = (int)(textureCoordinates.y * size);

	return _heightMap[iy][ix];
}


void Heightmap::getMinMaxHeights(float* min, float* max){
	(*min) = _heightMap[0][0];
	(*max) = _heightMap[0][0];

	for (int i = 0; i < _heightMap.size(); i++) {
		for (int j = 0; j < _heightMap[i].size(); j++) {
			if ((*min) > _heightMap[i][j]){
				(*min) = _heightMap[i][j];
			}
			if ((*max) < _heightMap[i][j]){
				(*max) = _heightMap[i][j];
			}
		}
	}
}


void Heightmap::generateAndSaveBiomeTexture(std::string path){

	//magic numbers
	float rockHeight = 0.25;
	float rockGrassHeight = 0.1;
	float snowBorder = 0.1;


	if(_mapType == NO_MAP){
		std::cerr<< "no map to save";
		return;
	}
	if (_mapType == SPHERE_MAP) {
		int size = int(sqrt(_vertices.size()));
		std::vector<unsigned char> buf(size * size * 4);
		if(_heightMap.size() == 0){
			fillUvsAndHeightmapFromSphereMap();
		}
		float minh, maxh, range;
		getMinMaxHeights(&minh, &maxh);
		range = maxh - _radius;

		for (int i = 0; i < _heightMap.size(); i++) {
			for (int j = 0; j < _heightMap[i].size(); j++) {
				float height = _heightMap[j][i];
				char r = 0; //rock
				char g = 0; //grass
				char b = 0; //grass-rock
				char a = 0; //snow

				if (height > _radius + range * rockGrassHeight){
					b = 255;
				}else{
					g = 255;
				}
				 if (height > _radius + range * rockHeight){
					r = 255;
					g = 0;
					b = 0;
				}
				int randOffset = randomRange(-5,5);
				 if (j + randOffset < _heightMap.size() * snowBorder ||
					 j + randOffset > _heightMap.size() * (1-snowBorder)){
					r = 0;
					g = 0;
					b = 0;
					a = 255;
				}

				int idx = (i * size + j) * 4;
				buf[idx] = r;
				buf[idx + 1] = g;
				buf[idx + 2] = b;
				buf[idx + 3] = a;
			}
		}

		// blending
//		{
//			int max_iters = 3;
//			for (int bl = 0; bl < max_iters; bl++) {
//				for (int i = 1; i < size - 1; i++) {
//					for (int j = 1; j < size - 1; j++) {
//						float neib[4] = {
//							_biomeMap[i + 1][j],
//							_biomeMap[i - 1][j],
//							_biomeMap[i][j + 1],
//							_biomeMap[i][j - 1]};
//						_biomeMap[i][j] = (neib[0] + neib[1] + neib[2] + neib[3]) / (4 - neibZeros);
//					}
//				}
//			}
//		}

		int err = SOIL_save_image
			(
				path.c_str(),
				SOIL_SAVE_TYPE_PNG,
				size, size, 4,
				&buf[0]
			);
		if (err != 0) {
			std::cerr<<"SOIL: " << SOIL_last_result() << "\n";
		}

	}
	else
	{
		std::cerr<< "bad map type";
		return;
	}
}

