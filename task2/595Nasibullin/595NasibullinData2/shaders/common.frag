#version 330

uniform sampler2D snowTex;
uniform sampler2D grassTex;
uniform sampler2D rockTex;
uniform sampler2D dirtTex;
uniform sampler2D mapTex;

const float snowSpecular = 1.0;
const float grassSpecular = 0.3;
const float rockSpecular = 0.1;
const float dirtSpecular = 0.2;

uniform int tileCount = 100;


struct LightInfo
{
	vec3 pos; //положение источника света в мировой системе координат
	int w ; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

void main()
{
	vec2 pixelCoord = texCoord * tileCount;
	vec4 pixelColor1 = texture(rockTex, pixelCoord);
	vec4 pixelColor2 = texture(grassTex, pixelCoord);
	vec4 pixelColor3 = texture(dirtTex, pixelCoord);
	vec4 pixelColor4 = texture(snowTex, pixelCoord);
	vec4 alphaMap = texture(mapTex, texCoord);


	vec4 blendedColor = pixelColor1 * alphaMap.r + pixelColor2 * alphaMap.g +
	               pixelColor3 * alphaMap.b + pixelColor4 * alphaMap.a;
	vec3 diffuseColor = blendedColor.rgb;

	// блики
	float blendedSpecular = (rockSpecular * alphaMap.r + grassSpecular * alphaMap.g +
         	               dirtSpecular * alphaMap.b + snowSpecular * alphaMap.a);
    Ks = vec3(blendedSpecular, blendedSpecular, blendedSpecular);

	vec3 lightDirCamSpace;
	if (light.w == 0){
		lightDirCamSpace = light.pos;
	}
	else
	{
		lightDirCamSpace = light.pos - posCamSpace.xyz; //направление на источник света
	}
	float distance = length(lightDirCamSpace);
	lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света

	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	
	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	vec3 color = diffuseColor * (light.La + light.Ld * NdotL);

	if (NdotL > 0.0)
	{			
		vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну				
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		
		color += light.Ls * Ks * blinnTerm;
	}

	fragColor = vec4(color, 0.5);
}
